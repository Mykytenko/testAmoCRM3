<?php

$name = htmlspecialchars($_POST['name'], ENT_NOQUOTES, 'UTF-8'); #Преобразование специальных HTML-объектов
$phone = htmlspecialchars($_POST['phone'], ENT_NOQUOTES, 'UTF-8');
$email = htmlspecialchars($_POST['email'], ENT_NOQUOTES, 'UTF-8');

$subdomain ='imykytenko7'; #Наш аккаунт - поддомен
$user = array(
    'USER_LOGIN' => 'i.mykytenko7@gmail.com', #Ваш логин (электронная почта)
    'USER_HASH' => '33289945', #Хэш для доступа к API (смотрите в профиле пользователя)
);
$IdPhoneField = '291363'; #Id поля телефон
$IdEmailField = '291365'; #Id поля мейл
$IdResponsible = '456979'; #Id поля ответственного за сделки

$dealName = 'Заявка с сайта'; #Название сделки
$dealStatusId = '33289945'; #
$dealSale = '1'; #Сума сделки
$dealTags = ''; #Теги для сделки
$contactTags = ''; #Теги для контактов


/**
 *
 * После успешной авторизации ищем контакт из email.
 * Если контакт не найден, то создаем новую карточку контакта и крепим к нему созданную сделку.
 * Если контакт был найден, то к данному контакту создаем новый сделку.
 *
 */

if (authorize($user, $link) > 0) {
    $contactInfo = findContact($subdomain, $email);
    $idContact = $contactInfo['idContact'];
    if ($idContact != null) {
        $idDeal[] = addDeal($dealName, $dealStatusId, $dealSale, $IdResponsible , $dealTags, $subdomain);
        if ($idDeal != null) {
            if (!empty($contactInfo['idLeads'])) {
                foreach ($contactInfo['idLeads'] as $idLeads) {
                    $idDeal[] = $idLeads;
                }
            }
            echo $idDeal;
            editContact($idContact, $idDeal, $subdomain);
        }
        echo $idDeal;
        addTask($idDeal, $IdResponsible, $subdomain);
    } else {
        addContact($name,$IdResponsible,$IdPhoneField,$phone,$IdEmailField,$email, $subdomain, $contactTags);
    }
}
header('Location: index.html');



/**Функуция авторизации
 * @param $user {array} - массив с логином пользователя и hash api ключем
 * @param $subdomain {string} - поддомен, по которому имеем доступ к amocrm
 * @return int - Если авторизовались = 1, если нет = -1.
 */
function authorize($user, $subdomain)
{
    #Формируем ссылку для запроса
    $link = 'https://' . $subdomain . '.amocrm.ru/private/api/auth.php?type=json';
    /* Нам необходимо инициировать запрос к серверу. Воспользуемся библиотекой cURL (поставляется в составе PHP). Вы также
    можете использовать и кроссплатформенную программу cURL, если вы не программируете на PHP. */
    $curl = curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
    curl_setopt($curl, CURLOPT_URL, $link);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($user));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_COOKIEFILE, dirname
        (__FILE__) . '/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl, CURLOPT_COOKIEJAR, dirname
        (__FILE__) . '/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    $out = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
    curl_close($curl); #Завершаем сеанс cURL
    /* Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
    $code = (int)$code;
    $errors = array(
        301 => 'Moved permanently',
        400 => 'Bad request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not found',
        500 => 'Internal server error',
        502 => 'Bad gateway',
        503 => 'Service unavailable',
    );
    try {
        #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
        if ($code != 200 && $code != 204) {
            throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        }

    } catch (Exception $E) {
        die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
    }
    /*
    Данные получаем в формате JSON, поэтому, для получения читаемых данных,
    нам придётся перевести ответ в формат, понятный PHP
     */
    $Response = json_decode($out, true);
    $Response = $Response['response'];
    if (isset($Response['auth']))
    {
        return 1;
    }
    return -1;
}

/**
 * Функция привязывания контакта к сделке
 * @param $idContact {number} - ID контакта
 * @param $idDeal {number} - ID сделки
 * @param $subdomain {string} - поддомен для доступа к amocrm
 * @return int - id измененного пользователя.
 */
function editContact($idContact, $idDeal, $subdomain)
{

    $contacts['update'] = array(
        array(
            'id' => $idContact,
            'updated_at' => time(),
            'leads_id' => $idDeal,
        )
    );

    $link = 'https://' . $subdomain . '.amocrm.ru/api/v2/contacts';

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
    curl_setopt($curl, CURLOPT_URL, $link);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($contacts));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
    curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    $out = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    $code = (int)$code;
    $errors = array(
        301 => 'Moved permanently',
        400 => 'Bad request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not found',
        500 => 'Internal server error',
        502 => 'Bad gateway',
        503 => 'Service unavailable'
    );
    try {
        if ($code != 200 && $code != 204) {
            throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        }
    } catch (Exception $E) {
        die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
    }
    $Response = json_decode($out, true);
    $Response = $Response['_embedded']['items'][0]['id'];
    return $Response;
}


/**Функция создания новой сделки
 * @param $subdomain
 * @param $IdResponsible
 * @return mixed
 */
function addDeal($dealName, $dealStatusId, $dealSale, $IdResponsible, $dealTags, $subdomain)
{
    $leads['add'] = array(
        array(
            'name' => $dealName,
            'created_at' => time(),
            'status_id' => $dealStatusId,
            'sale' => $dealSale,
            'responsible_user_id' => $IdResponsible,
            'tags' => $dealTags
        ),
    );

    $link = 'https://' . $subdomain . '.amocrm.ru/api/v2/leads';
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
    curl_setopt($curl, CURLOPT_URL, $link);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($leads));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
    curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    $out = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    $code = (int)$code;
    $errors = array(
        301 => 'Moved permanently',
        400 => 'Bad request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not found',
        500 => 'Internal server error',
        502 => 'Bad gateway',
        503 => 'Service unavailable'
    );
    try {
        if ($code != 200 && $code != 204) {
            throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        }
    } catch (Exception $E) {
        die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
    }
    $Response = json_decode($out, true);
    $Response = $Response['_embedded']['items'][0]['id'];
    return $Response;
}

/**
 * Функция добавления нового контакта. Привязывается мобильный телефон и рабочий Email.
 * @param $name {string} - имя пользователя
 * @param $IdResponsible {number} - ID ответственного
 * @param $IdPhoneField {number} - ID Телефон
 * @param $phone {string} - телефон пользователя
 * @param $IdEmailField {number} - ID Email
 * @param $email {string} - email пользователя
 * @param $subdomain {string} - поддомен для доступа
 * @return int - ID добавленного пользователя
 */

function addContact($name, $IdResponsible, $IdPhoneField, $phone, $IdEmailField, $email, $subdomain, $contactTags)
{
    $contacts['add'] = array(
        array(
            'name' => $name,
            'responsible_user_id' => $IdResponsible,
            'created_by' => $IdResponsible,
            'created_at' => time(),
            'tags' => $contactTags, //Теги
            'custom_fields' => array(
                array(
                    'id' => "$IdPhoneField",
                    'values' => array(
                        array(
                            'value' => "$phone",
                            'enum' => "MOB"
                        )
                    )
                ),
                array(
                    'id' => $IdEmailField,
                    'values' => array(
                        array(
                            'value' => $email,
                            'enum' => "WORK"
                        )
                    )
                ),
            ),
        )
    );
    $link = '';
    $link .= 'https://' . $subdomain . '.amocrm.ru/api/v2/contacts';

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
    curl_setopt($curl, CURLOPT_URL, $link);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($contacts));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
    curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    $out = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    $code = (int)$code;
    $errors = array(
        301 => 'Moved permanently',
        400 => 'Bad request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not found',
        500 => 'Internal server error',
        502 => 'Bad gateway',
        503 => 'Service unavailable'
    );
    try {
        if ($code != 200 && $code != 204) {
            throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        }
    } catch (Exception $E) {
        die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
    }

    $Response = json_decode($out, true);
    $Response = $Response['_embedded']['items'][0]['id'];
    return $Response;
}


/**
с типом “Перезвонить клиенту”, ответственный у задачи должен быть такой же как и у сделки,
 * а срок выполнения задачи 1 день;
 * @param $idDeal {number} - ID сделки
 * @param $IdResponsible {number} - ID ответственного
 * @param $subdomain {string} - поддомен для доступа к amocrm
 * @return int - id измененного пользователя.
 */

function addTask( $idDeal, $IdResponsible, $subdomain)
{

    $tasks['add'] = array(
        array(
            'leads_id' => $idDeal,
            'task_type' => 1, #Звонок
            'text' => 'Перезвонить клиенту',
            'created_by' => $IdResponsible,
            'updated_at' => time()
        )
    );

    $link = 'https://' . $subdomain . '.amocrm.ru/api/v2/tasks';

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
    curl_setopt($curl, CURLOPT_URL, $link);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($tasks));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
    curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    $out = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    $code = (int)$code;
    $errors = array(
        301 => 'Moved permanently',
        400 => 'Bad request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not found',
        500 => 'Internal server error',
        502 => 'Bad gateway',
        503 => 'Service unavailable'
    );
    try {
        if ($code != 200 && $code != 204) {
            throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        }
    } catch (Exception $E) {
        die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
    }
    $Response = json_decode($out, true);
    $Response = $Response['_embedded']['items'][0]['id'];
    return $Response;
}



/**
 * Функция поиска контакта
 * @param $subdomain {string} - поддомен для доступа к amocrm
 * @param $email {string} - email пользователя
 * @return array - массив с id пользователя и со списком сделок, к которым он привязан
 */
function findContact($subdomain, $email)
{
    $link = 'https://' . $subdomain . '.amocrm.ru/api/v2/contacts/?query=' . $email;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
    curl_setopt($curl, CURLOPT_URL, $link);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
    curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    $out = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    $code = (int)$code;
    $errors = array(
        301 => 'Moved permanently',
        400 => 'Bad request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not found',
        500 => 'Internal server error',
        502 => 'Bad gateway',
        503 => 'Service unavailable'
    );
    try {
        if ($code != 200 && $code != 204) {
            throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        }
    } catch (Exception $E) {
        die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
    }

    $Response = json_decode($out, true);
    $Response = $Response['_embedded']['items'][0];
    $Response['idContact'] = $Response['id'];
    $Response['idLeads'] = $Response['leads']['id'];
    return $Response;
}

