function validate(){
    var name = document.getElementById("name").value;
    var phone = document.getElementById("phone").value;
    var email = document.getElementById("email").value;
    var error_message = document.getElementById("error_message");

    error_message.style.padding = "10px";

    var text;
    if(name.length < 4){
        text = "Пожалуйста, введите действительное имя";
        error_message.innerHTML = text;
        return false;
    }

    if(isNaN(phone) || phone.length != 10){
        text = "Пожалуйста, введите действительный номер телефона";
        error_message.innerHTML = text;
        return false;
    }
    if(email.indexOf("@") == -1 || email.length < 6){
        text = "Пожалуйста, введите действительный адрес электронной почты";
        error_message.innerHTML = text;
        return false;
    }
    alert("Форма Успешно Отправлена!");
    return true;
}